package com.myapp.broker;

import java.util.Queue;
import java.util.Scanner;

public class UserInput implements IUserInput {

    public Queue<String> userMessageQueue;

    private String getUserMessage() {
        String userMessage = "";
        Scanner in = new Scanner(System.in);
        userMessage = in.nextLine();
        return userMessage;
    }

    private void addMessageToQueue() {
        while (true) {
            System.out.println("Enter message:");
            String message = getUserMessage();
            userMessageQueue.add(message);
        }
    }

    @Override
    public void clearQueue() {
        userMessageQueue.clear();
    }

    @Override
    public Queue<String> getQueue() {
        return userMessageQueue;
    }

    public static void main(String[] args) {
        UserInput myClient = new UserInput();
        myClient.addMessageToQueue();
    }
}
