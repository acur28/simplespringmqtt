package com.myapp.broker;

import java.util.Queue;

public interface IUserInput {

    //Queue<String> userMessageQueue = null;
    // public String getUserMessage();
    // public void addMessageToQueue();
    void clearQueue();
    Queue<String> getQueue();
    // public void printQueueContent();

}
